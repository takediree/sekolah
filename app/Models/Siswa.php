<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $table ='siswa';

    protected $fillable = ['kelas','nis','nama','jenis_kelamin','alamat','ttl','wali','hp'];
    
}
