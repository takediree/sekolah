<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\Sanksi;
use App\Models\Pelanggaran;


use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        $jumlahsiswa = Siswa::count();
        $jumlahkelas = Kelas::count();
        $jumlahsanksi = Sanksi::count();
        $jumlahpelanggaran = Pelanggaran::count();
     return view('admin/index',compact('jumlahsiswa','jumlahsanksi','jumlahpelanggaran','jumlahkelas'));
    }
    
    public function guru()
    {
        $jumlahsiswa = Siswa::count();
        $jumlahkelas = Kelas::count();
        $jumlahsanksi = Sanksi::count();
        $jumlahpelanggaran = Pelanggaran::count();
     return view('guru/index',compact('jumlahsiswa','jumlahsanksi','jumlahpelanggaran','jumlahkelas'));
    
    }
    
    public function wali()
    {
     return view('wali/index');
    }


    //gurupiket
    public function gurupiket()
    {
        $user = User::where('role','=',2)->get();

     return view('admin/guru/index',compact('user'));
    }
    public function deletegurupiket($id)
    {
        $p = User::find($id);
        $p->delete();
        return redirect('admin/guru')->with('status', 'Data berhasil update!');
    }

    //walisiswa
    public function walimurid()
    {
        $wali = Siswa::all();
        return view('admin/wali/index',compact('wali'));
    }
    

    


}
