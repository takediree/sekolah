<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\Kelas;
use Illuminate\Http\Request;
// use App\Exports\UsersExport;
use App\Imports\SiswaImport;
use Maatwebsite\Excel\Facades\Excel;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = Siswa::all();

        $kelas = Siswa::join('kelas','kelas.id', '=', 'siswa.kelas')
                        ->select('kelas.kelas as kelas')
                        ->first();

        return view('admin.siswa.index',compact('siswa','kelas'));
    }
   
    
    public function upload()
    {
        
        return view('admin.siswa.upload');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $kelas = Kelas::all();
        return view('admin.siswa.create',compact('kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelas' => 'required',
            'nis' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'ttl' => 'required',
            'wali' => 'required',
            'hp' => 'required|numeric',
           
            
        ]);

            
            $siswa = new Siswa;

        
            $siswa->kelas = $request->kelas;    
            $siswa->nis = $request->nis;    
            $siswa->nama = $request->nama;    
            $siswa->jenis_kelamin = $request->jenis_kelamin;    
            $siswa->alamat = $request->alamat;    
            $siswa->ttl = $request->ttl;    
            $siswa->wali = $request->wali;    
            $siswa->hp = $request->hp;    
            
            $siswa->save();
            
            return redirect('admin/siswa')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        
        $kelas = Kelas::all();
        return view('admin.siswa.update',compact('kelas','siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        $request->validate([
            'kelas' => 'required',
            'nis' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'ttl' => 'required',
            'wali' => 'required',
            'hp' => 'required|numeric',
           
            
        ]);

            
            $siswa = Siswa::find($siswa->id);

        
            $siswa->kelas = $request->kelas;    
            $siswa->nis = $request->nis;    
            $siswa->nama = $request->nama;    
            $siswa->jenis_kelamin = $request->jenis_kelamin;    
            $siswa->alamat = $request->alamat;    
            $siswa->ttl = $request->ttl;    
            $siswa->wali = $request->wali;    
            $siswa->hp = $request->hp;    
            
            $siswa->save();
            
            
            return redirect('admin/siswa')->with('status', 'Data berhasil update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $s = Siswa::find($id);
        $s->delete();
        return redirect('admin/siswa')->with('status', 'Data berhasil update!');
    }


    public function import() 
    {
        \Excel::import(new SiswaImport,request()->file('import'));
             
        return redirect('admin/siswa')->with('status', 'Data berhasil tambahkan');
    }
}
