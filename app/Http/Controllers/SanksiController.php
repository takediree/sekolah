<?php

namespace App\Http\Controllers;
use App\Models\NewTambahPelanggaran;
use App\Models\Sanksi;
use App\Models\Siswa;
use Illuminate\Http\Request;

class SanksiController extends Controller
{
  
    public function index()
    {
        $sanksi = Sanksi::all();
        return view('admin.sanksi.index',compact('sanksi'));
    }



    public function create()
    {
        return view('admin.sanksi.create');
    }



    public function store(Request $request)
    {
        $request->validate([
            'sanksi' => 'required',
            'poin' => 'required',
            
        ]);

            
            $sanksi = new Sanksi;

        
            $sanksi->sanksi = $request->sanksi;    
            $sanksi->poin = $request->poin;
            $sanksi->poin_batas = $request->poin_batas;
            $sanksi->save();
            
            return redirect('admin/sanksi')->with('status', 'Data berhasil ditambahkan!');
    }


    public function show(Sanksi $sanksi)
    {
        //
    }

    public function edit(Sanksi $sanksi)
    {
       
        return view('admin.sanksi.update',compact('sanksi'));
    }

    public function update(Request $request, Sanksi $sanksi)
    {
        $request->validate([
            'sanksi' => 'required',
            'poin' => 'required',
            
        ]);

            
            $sanksi = Sanksi::find($sanksi->id);

        
            $sanksi->sanksi = $request->sanksi;    
            $sanksi->poin = $request->poin;
            $sanksi->save();
            
            return redirect('admin/sanksi')->with('status', 'Data berhasil update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sanksi  $sanksi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $s = Sanksi::find($id);
        $s->delete();
        return redirect('admin/sanksi')->with('status', 'Data berhasil update!');
    }

    public function rekomendasi(){

        $data = array();
        $data_pelanggaran = array();
        $data_ternormalisasi = array();
        $data_ternormalisasi_bobot = array();

        $pelanggaran = array();
        $pelanggaran_ternormalisasi = array();
        $ternormalisasi_bobot = array();
        $siswa = Siswa::all();

        foreach ($siswa as $s){
            //Ambil Kerajinan
            $kerajinan= NewTambahPelanggaran::where("id_siswa"  , '=', $s->id)
                                                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'newdatapelanggaran.id_pelanggaran')
                                                ->where('pelanggaran.kategori' ,'=' , 4)
                                                ->count();

            //Ambil Kebersihan
            $kebersihan= NewTambahPelanggaran::where("id_siswa"  , '=', $s->id)
                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'newdatapelanggaran.id_pelanggaran')
                ->where('pelanggaran.kategori' ,'=' , 5)
                ->count();

            //Ambil Sikap
            $sikap= NewTambahPelanggaran::where("id_siswa"  , '=', $s->id)
                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'newdatapelanggaran.id_pelanggaran')
                ->where('pelanggaran.kategori' ,'=' , 1)
                ->count();

            $pelanggaran = [
                "nama" => $s->nama,
                "kerajinan" => $kerajinan,
                "kebersihan" => $kebersihan,
                "sikap" => $sikap
            ];

            $data_pelanggaran[] = $pelanggaran;

        }

        foreach ($data_pelanggaran as $d) {

            $jumlah_pembagi_kerajinan = 0;
            $jumlah_pembagi_kebersihan = 0;
            $jumlah_pembagi_sikap = 0;

            foreach ($data_pelanggaran as $dd) {
                $jumlah_pembagi_kerajinan += pow($dd["kerajinan"], 2);
                $jumlah_pembagi_kebersihan += pow($dd["kebersihan"], 2);
                $jumlah_pembagi_sikap += pow($dd["sikap"], 2);
            }

            $normalisasi = [
                "nama" => $d["nama"],
                "kerajinan" => ($d["kerajinan"]/$jumlah_pembagi_kerajinan),
                "kebersihan" => ($d["kebersihan"]/$jumlah_pembagi_kebersihan),
                "sikap" => ($d["sikap"]/$jumlah_pembagi_sikap)
            ];

            $data_ternormalisasi[] = $normalisasi;

        }

        foreach ($data_pelanggaran as $d) {

            $jumlah_pembagi_kerajinan = 0;
            $jumlah_pembagi_kebersihan = 0;
            $jumlah_pembagi_sikap = 0;

            foreach ($data_pelanggaran as $dd) {
                $jumlah_pembagi_kerajinan += pow($dd["kerajinan"], 2);
                $jumlah_pembagi_kebersihan += pow($dd["kebersihan"], 2);
                $jumlah_pembagi_sikap += pow($dd["sikap"], 2);
            }

            $normalisasi = [
                "nama" => $d["nama"],
                "kerajinan" => ($d["kerajinan"]/$jumlah_pembagi_kerajinan)*0.34,
                "kebersihan" => ($d["kebersihan"]/$jumlah_pembagi_kebersihan)*0.49,
                "sikap" => ($d["sikap"]/$jumlah_pembagi_sikap)+0.16
            ];

            $data_ternormalisasi_bobot[] = $normalisasi;

        }

        $data = [
            "pelanggaran" => $data_pelanggaran,
            "normalisasi" => $data_ternormalisasi,
            "normalisasi_bobot" => $data_ternormalisasi_bobot
        ];

        $sanksi = (object) $data;


        //Solusi Ideal Positif


        $datasolusi = array();

        $solusi_kerajinan = array();
        $solusi_kebersihan = array();
        $solusi_sikap = array();

        $solusi_ideal_positif = array();
        $solusi_ideal_negatif = array();


        foreach ($data_ternormalisasi_bobot as $d) {


            $solusi = array();


            $solusi_kerajinan[] = $d["kerajinan"];
            $solusi_kebersihan[] = $d["kebersihan"];
            $solusi_sikap[] = $d["sikap"];


            $solusi[] = $d["kerajinan"];
            $solusi[] = $d["kebersihan"];
            $solusi[] = $d["sikap"];

            $data = [
                'solusi_ideal' => max($solusi),
                'data' => $d["nama"]
            ];

            $datasolusi[] = $data;

        }


        $solusi_ideal_positif[] = max($solusi_kerajinan);
        $solusi_ideal_positif[] = max($solusi_kebersihan);
        $solusi_ideal_positif[] = max($solusi_sikap);


        $solusi_ideal_negatif[] = min($solusi_kerajinan);
        $solusi_ideal_negatif[] = min($solusi_kebersihan);
        $solusi_ideal_negatif[] = min($solusi_sikap);


        //Menghitung Jarak
        $jarak_positif = array();
        $jarak_negatif = array();

        for($x=0; $x < sizeof($solusi_kerajinan); $x++){

            $d_1 = pow(max($solusi_kerajinan)-$solusi_kerajinan[$x] , 2);
            $d_2 = pow(max($solusi_kebersihan)-$solusi_kebersihan[$x] , 2);
            $d_3 = pow(max($solusi_sikap)-$solusi_sikap[$x] , 2);

            $jarak_positif[]  = $d_1+$d_2+$d_3;
        }

        for($x=0; $x < sizeof($solusi_kerajinan); $x++){

            $d_1 = pow(min($solusi_kerajinan)-$solusi_kerajinan[$x] , 2);
            $d_2 = pow(min($solusi_kebersihan)-$solusi_kebersihan[$x] , 2);
            $d_3 = pow(min($solusi_sikap)-$solusi_sikap[$x] , 2);

            $jarak_negatif[]  = $d_1+$d_2+$d_3;
        }


        //Nilai Preferensi
        $prefrensi = array();

        for($x=0; $x < sizeof($jarak_positif); $x++){


            $v = $jarak_negatif[$x]/($jarak_negatif[$x]+$jarak_positif[$x]);


            $prefrensi[] = $v;

        }


        //Ambil Sanksi Terdekat Sesuai Preferensi
        $db_sanksi = Sanksi::all();

        $data_poin_batas = array();

        foreach($db_sanksi as $db){

            $data_poin_batas[] = $db->poin_batas;

        }

        $rekomendasi = array();
        foreach ($prefrensi as $p){


            $value = $this->getClosest($p, $data_poin_batas );
            $index = array_search($value , $data_poin_batas);


            $rekomendasi[] = $db_sanksi[$index]->sanksi;


        }

        return view('admin/sanksi/rekomendasi' , compact("sanksi" , "prefrensi" , "rekomendasi"));
    }


    function getClosest($search, $arr) {
        $closest = null;
        foreach ($arr as $item) {
            if ($closest === null || abs($search - $closest) > abs($item - $search)) {
                $closest = $item;
            }
        }
        return $closest;
    }


    public function deletedatapelanggaran()
    {        
            $s = NewTambahPelanggaran::find($id);
            $s->delete();
            return redirect('admin/sanksi/rekomendasi')->with('status', 'Data berhasil update!');
    }
}

