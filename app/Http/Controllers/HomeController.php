<?php

namespace App\Http\Controllers;
use App\Models\Bengkel;
use App\Models\Kategori;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function home(   )
    {
        
        
        $bengkel = Bengkel::all();
        $kategori = Kategori::all();
       
        return view('user/home',compact('bengkel','kategori'));
    }
    
    public function list()
    {
        $jumlahbengkel = Bengkel::count();
        $kategori = Kategori::all();
        $bengkel = Bengkel::paginate(5);
        return view('user/list',compact('bengkel','kategori',"jumlahbengkel"));
    }
    
    public function map()
    {
        $kategori = Kategori::all();
        $bengkel = Bengkel::all();
      
        return view('user/map',compact('bengkel','kategori'));
    }
    
    
    public function bengkel(Request $request)
    {
        $id = $request->get("id");
        $bengkel = Bengkel::all();
        $kategori = Kategori::all();
        
        $listbengkel = Bengkel::where('kategori' , '=' , $id)->get();
        return view('user/bengkel',compact('bengkel','kategori','listbengkel'));
    }

    public function search(Request $request)
    {
       
        $jumlahbengkel = Bengkel::count();
       $kategori = Kategori::all();

        $key = $request->search;

      
        $searchbengkel = Bengkel::
            where('nama', 'like', "%{$key}%")
            ->paginate(5);

        return view('/user/search', compact('key','searchbengkel','kategori','jumlahbengkel'));
    }
}