<?php

namespace App\Http\Controllers;

use App\Models\Datapelanggaran;
use App\Models\NewTambahPelanggaran;
use App\Models\Kelas;
use App\Models\Sanksi;
use App\Models\Siswa;
use App\Models\Pelanggaran;
use App\Models\Kategori;
use Illuminate\Http\Request;

class DatapelanggaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datapelanggaran = Datapelanggaran::all();
        $data = DataPelanggaran::join('siswa' , 'siswa.id' , '=' , 'datapelanggaran.nama')
                                ->join('kelas' , 'kelas.id' , '=' , 'siswa.kelas')
                                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'datapelanggaran.pelanggaran')
                                ->select('siswa.nama as nama' ,'siswa.nis as nis' , 'pelanggaran.pelanggaran as pelanggaran',
                                        'siswa.kelas as kelas'  )
                                ->get();
        dd($data);
        return view('admin.datapelanggaran.index',compact('datapelanggaran','data'));
    }
    public function kategori()
    {
        $sanksi = Sanksi::all();
        $kategori = Kategori::all();
        return view('guru.inputpelanggaran.kategori',compact('sanksi','kategori'));
    }
    
    public function pelanggaran()
    {
       
        $data = DataPelanggaran::join('siswa' , 'siswa.id' , '=' , 'datapelanggaran.nama')
                                ->join('kelas' , 'kelas.id' , '=' , 'siswa.kelas')
                                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'datapelanggaran.pelanggaran')
                                ->select('siswa.nama as nama' ,'siswa.nis as nis' , 'pelanggaran.pelanggaran as pelanggaran',
                                          'siswa.kelas as kelas'  )
                                ->get();

        $sanksi = Sanksi::all();
        $kategori = Kategori::all();
        $siswa = Siswa::all();
        $pelanggaran = Pelanggaran::all();
        
        

        

        return view('guru.inputpelanggaran.pelanggaran',compact('sanksi','kategori','siswa','pelanggaran'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::all();
        $siswa = Siswa::all();

        return view('guru.inputpelanggaran.create',compact('kelas','siswa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Datapelanggaran  $datapelanggaran
     * @return \Illuminate\Http\Response
     */
    public function show(Datapelanggaran $datapelanggaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Datapelanggaran  $datapelanggaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Datapelanggaran $datapelanggaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Datapelanggaran  $datapelanggaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datapelanggaran $datapelanggaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Datapelanggaran  $datapelanggaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $s = NewTambahPelanggaran::find($id);
        $s->delete();
        return redirect('admin/siswa')->with('status', 'Data berhasil update!');
    }

    public function tambahpelanggaran(){


        $siswa = Siswa::all();
        $pelanggaran = Pelanggaran::all();

        return view('admin/datapelanggaran/tambahpelanggaransiswa' , compact('siswa' , 'pelanggaran'));

    }
    public function simpanpelanggaransiswa(Request $request){

        $id_siswa = $request->id_siswa;
        $id_pelanggaran = $request->id_pelanggaran;


        $newdatapelanggaran = new NewTambahPelanggaran;
        $newdatapelanggaran->id_siswa       = $id_siswa;
        $newdatapelanggaran->id_pelanggaran = $id_pelanggaran;
        $newdatapelanggaran->save();


        return back()->with('status', 'Data berhasil ditambahkan!');



    }
}
