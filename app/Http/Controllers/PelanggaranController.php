<?php

namespace App\Http\Controllers;

use App\Models\Pelanggaran;
use App\Models\Kategori;
use Illuminate\Http\Request;

class PelanggaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $pelanggaran = Pelanggaran::all();
        $kategori = Pelanggaran::join('kategori','kategori.id', '=', 'pelanggaran.kategori')
                             ->select('kategori.nama as kategori','pelanggaran.pelanggaran as pelanggaran','pelanggaran.poin','pelanggaran.id')
                             ->get();
        return view('admin/pelanggaran/index',compact('pelanggaran','kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('admin/pelanggaran/create',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pelanggaran' => 'required',
            'poin' => 'required|numeric',
            'kategori' => 'required',
        ]);

            
            $pelanggaran = new Pelanggaran;

        
            $pelanggaran->pelanggaran = $request->pelanggaran;    
            $pelanggaran->poin = $request->poin;
            $pelanggaran->kategori = $request->kategori;  
            $pelanggaran->save();
            
            return redirect('admin/pelanggaran')->with('status', 'Data berhasil ditambahkan!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pelanggaran  $pelanggaran
     * @return \Illuminate\Http\Response
     */
    public function show(Pelanggaran $pelanggaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pelanggaran  $pelanggaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelanggaran $pelanggaran)
    {
        $kategori = Kategori::all();
        return view('admin.pelanggaran.update',compact('pelanggaran','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pelanggaran  $pelanggaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pelanggaran $pelanggaran)
    {
        $request->validate([
            'pelanggaran' => 'required',
            'poin' => 'required',
            'kategori'=> 'required'
        ],

    
    );

            
            $pelanggaran= Pelanggaran::find($pelanggaran->id);

        
            $pelanggaran->pelanggaran = $request->pelanggaran;    
            $pelanggaran->poin = $request->poin;
            $pelanggaran->kategori = $request->kategori;  
            $pelanggaran->save();
            
            return redirect('admin/pelanggaran')->with('status', 'Data berhasil update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelanggaran  $pelanggaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Pelanggaran::find($id);
        $p->delete();
        return redirect('admin/pelanggaran')->with('status', 'Data berhasil dihapus!');
    }
}
