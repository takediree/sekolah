<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    //
    public function login()
    {
        return view('/login');
    }


    public function authenticate(Request $request)
    {
        

        try {
           
            if (Auth::attempt(['nip' => $request->nip, 'password' => $request->password])) {    

                $user=Auth::user();

                session(['nip' => $user->nip]);
                session(['id' => $user->id]);
                session(['nama' => $user->nama]);
                


                if(auth()->user()->role == 1){

                    return Redirect::to('/admin');

                }elseif(auth()->user()->role == 2){

                    return Redirect::to('/guru');

                }elseif(auth()->user()->role == 3){

                    return Redirect::to('/wali');

                }


            } else {

            
                session()->flash("pesan" , "Password / Email Salah");
                return Redirect::to('/');

            
            }



        }catch(\Exception $e){
            print_r($e->getMessage());
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
      }
}
