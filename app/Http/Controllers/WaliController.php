<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pesan;
use App\Models\Siswa;
use App\Models\Sanksi;
use App\Models\NewTambahPelanggaran;

class WaliController extends Controller
{
    public function index()
    {
        $id = session()->get('id');

        

        $pesan = Pesan::where('id_pengirim','=',$id)->get();
        
    
        return view('wali.pesan.index',compact('pesan'));
    }
    public function createpesan()
    {
        $user = User::where('role','=',2)->get('id');
        return view('wali.pesan.kirim',compact('user'));
    }


    public function storepesan(Request $request)
    {
        $request->validate([
            'notifikasi' => 'null',
            
        ],
    );
         

        $pesan = new Pesan ;
        $pesan->subjek = $request->subjek;
        $pesan->pesan = $request->pesan;
        $pesan->id_penerima = $request->id_penerima;
        $pesan->id_pengirim =$request->session()->get('id');
        $pesan->notifikasi = 0;

        $pesan->save();

        return redirect('wali/pesan/kirim')->with('status', 'pesan berhasil dikirim!');
      
    }
    // public function balas()
    // {
        
    //     $user = User::where('role','=',2)->get();


    //     return view('wali.pesan.balas',compact('user'));

    // }

    // public function balaspesan(Request $request, Pesan $id_penerima)
    // {
    //     $request->validate([
    //         'notifikasi' => 'null',
            
    //     ],
    // );
         

    //     $pesan = new Pesan ;
    //     $pesan->subjek = $request->subjek;
    //     $pesan->pesan = $request->pesan;
    //     $pesan->id_penerima = $request->session()->get('id');
    //     $pesan->id_pengirim =$id_pengirim;
    //     $pesan->notifikasi = 0;

    //     $pesan->save();

    //     return redirect('wali/pesan/show')->with('status', 'pesan berhasil dikirim!');
      
    // }
  

    public function showpesan(Pesan $pesan )
    {
   
        $pengirim = Pesan::join('users', 'users.id', '=', 'pesan.id_penerima')
                           ->select('users.nama as nama')
                           ->first();

        return view('wali/pesan/show', compact('pesan','pengirim')); 
    }

    public function rekomendasi(){

        $data = array();
        $data_pelanggaran = array();
        $data_ternormalisasi = array();
        $data_ternormalisasi_bobot = array();

        $pelanggaran = array();
        $pelanggaran_ternormalisasi = array();
        $ternormalisasi_bobot = array();
        $siswa = Siswa::all();

        foreach ($siswa as $s){
            //Ambil Kerajinan
            $kerajinan= NewTambahPelanggaran::where("id_siswa"  , '=', $s->id)
                                                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'newdatapelanggaran.id_pelanggaran')
                                                ->where('pelanggaran.kategori' ,'=' , 4)
                                                ->count();

            //Ambil Kebersihan
            $kebersihan= NewTambahPelanggaran::where("id_siswa"  , '=', $s->id)
                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'newdatapelanggaran.id_pelanggaran')
                ->where('pelanggaran.kategori' ,'=' , 5)
                ->count();

            //Ambil Sikap
            $sikap= NewTambahPelanggaran::where("id_siswa"  , '=', $s->id)
                ->join('pelanggaran' , 'pelanggaran.id' , '=' , 'newdatapelanggaran.id_pelanggaran')
                ->where('pelanggaran.kategori' ,'=' , 1)
                ->count();

            $pelanggaran = [
                "nama" => $s->nama,
                "kerajinan" => $kerajinan,
                "kebersihan" => $kebersihan,
                "sikap" => $sikap
            ];

            $data_pelanggaran[] = $pelanggaran;

        }

        foreach ($data_pelanggaran as $d) {

            $jumlah_pembagi_kerajinan = 0;
            $jumlah_pembagi_kebersihan = 0;
            $jumlah_pembagi_sikap = 0;

            foreach ($data_pelanggaran as $dd) {
                $jumlah_pembagi_kerajinan += pow($dd["kerajinan"], 2);
                $jumlah_pembagi_kebersihan += pow($dd["kebersihan"], 2);
                $jumlah_pembagi_sikap += pow($dd["sikap"], 2);
            }

            $normalisasi = [
                "nama" => $d["nama"],
                "kerajinan" => ($d["kerajinan"]/$jumlah_pembagi_kerajinan),
                "kebersihan" => ($d["kebersihan"]/$jumlah_pembagi_kebersihan),
                "sikap" => ($d["sikap"]/$jumlah_pembagi_sikap)
            ];

            $data_ternormalisasi[] = $normalisasi;

        }

        foreach ($data_pelanggaran as $d) {

            $jumlah_pembagi_kerajinan = 0;
            $jumlah_pembagi_kebersihan = 0;
            $jumlah_pembagi_sikap = 0;

            foreach ($data_pelanggaran as $dd) {
                $jumlah_pembagi_kerajinan += pow($dd["kerajinan"], 2);
                $jumlah_pembagi_kebersihan += pow($dd["kebersihan"], 2);
                $jumlah_pembagi_sikap += pow($dd["sikap"], 2);
            }

            $normalisasi = [
                "nama" => $d["nama"],
                "kerajinan" => ($d["kerajinan"]/$jumlah_pembagi_kerajinan)*0.34,
                "kebersihan" => ($d["kebersihan"]/$jumlah_pembagi_kebersihan)*0.49,
                "sikap" => ($d["sikap"]/$jumlah_pembagi_sikap)+0.16
            ];

            $data_ternormalisasi_bobot[] = $normalisasi;

        }

        $data = [
            "pelanggaran" => $data_pelanggaran,
            "normalisasi" => $data_ternormalisasi,
            "normalisasi_bobot" => $data_ternormalisasi_bobot
        ];

        $sanksi = (object) $data;


        //Solusi Ideal Positif


        $datasolusi = array();

        $solusi_kerajinan = array();
        $solusi_kebersihan = array();
        $solusi_sikap = array();

        $solusi_ideal_positif = array();
        $solusi_ideal_negatif = array();


        foreach ($data_ternormalisasi_bobot as $d) {


            $solusi = array();


            $solusi_kerajinan[] = $d["kerajinan"];
            $solusi_kebersihan[] = $d["kebersihan"];
            $solusi_sikap[] = $d["sikap"];


            $solusi[] = $d["kerajinan"];
            $solusi[] = $d["kebersihan"];
            $solusi[] = $d["sikap"];

            $data = [
                'solusi_ideal' => max($solusi),
                'data' => $d["nama"]
            ];

            $datasolusi[] = $data;

        }


        $solusi_ideal_positif[] = max($solusi_kerajinan);
        $solusi_ideal_positif[] = max($solusi_kebersihan);
        $solusi_ideal_positif[] = max($solusi_sikap);


        $solusi_ideal_negatif[] = min($solusi_kerajinan);
        $solusi_ideal_negatif[] = min($solusi_kebersihan);
        $solusi_ideal_negatif[] = min($solusi_sikap);


        //Menghitung Jarak
        $jarak_positif = array();
        $jarak_negatif = array();

        for($x=0; $x < sizeof($solusi_kerajinan); $x++){

            $d_1 = pow(max($solusi_kerajinan)-$solusi_kerajinan[$x] , 2);
            $d_2 = pow(max($solusi_kebersihan)-$solusi_kebersihan[$x] , 2);
            $d_3 = pow(max($solusi_sikap)-$solusi_sikap[$x] , 2);

            $jarak_positif[]  = $d_1+$d_2+$d_3;
        }

        for($x=0; $x < sizeof($solusi_kerajinan); $x++){

            $d_1 = pow(min($solusi_kerajinan)-$solusi_kerajinan[$x] , 2);
            $d_2 = pow(min($solusi_kebersihan)-$solusi_kebersihan[$x] , 2);
            $d_3 = pow(min($solusi_sikap)-$solusi_sikap[$x] , 2);

            $jarak_negatif[]  = $d_1+$d_2+$d_3;
        }


        //Nilai Preferensi
        $prefrensi = array();

        for($x=0; $x < sizeof($jarak_positif); $x++){


            $v = $jarak_negatif[$x]/($jarak_negatif[$x]+$jarak_positif[$x]);


            $prefrensi[] = $v;

        }


        //Ambil Sanksi Terdekat Sesuai Preferensi
        $db_sanksi = Sanksi::all();

        $data_poin_batas = array();

        foreach($db_sanksi as $db){

            $data_poin_batas[] = $db->poin_batas;

        }

        $rekomendasi = array();
        foreach ($prefrensi as $p){


            $value = $this->getClosest($p, $data_poin_batas );
            $index = array_search($value , $data_poin_batas);


            $rekomendasi[] = $db_sanksi[$index]->sanksi;


        }

        return view('wali/index' , compact("sanksi" , "prefrensi" , "rekomendasi"));
    }
    function getClosest($search, $arr) {
        $closest = null;
        foreach ($arr as $item) {
            if ($closest === null || abs($search - $closest) > abs($item - $search)) {
                $closest = $item;
            }
        }
        return $closest;
    }
}
