<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\PelanggaranController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\SanksiController;
use App\Http\Controllers\DatapelanggaranController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\PesanController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\WaliController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [LoginController::class, 'login'])->name('login');
Route::post('/authenticate', [LoginController::class , 'authenticate']);

Route::get('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'register']);
Route::post('/register', [RegisterController::class, 'create_register']);

Route::group(['middleware' => 'auth'], function () {

Route::get('/admin', [AdminController::class, 'index']);
Route::get('/guru', [AdminController::class, 'guru']);
// Route::get('/wali', [AdminController::class, 'wali']);


// admin
//pelanggaran 
Route::get('/admin/pelanggaran', [PelanggaranController::class, 'index']);
Route::get('/admin/pelanggaran/create', [PelanggaranController::class, 'create']);
Route::get('/admin/pelanggaran/update/{pelanggaran}', [PelanggaranController::class, 'edit']);
Route::post('/admin/pelanggaran/', [PelanggaranController::class, 'store']);
Route::put('/admin/pelanggaran/{pelanggaran}', [PelanggaranController::class, 'update']);
Route::get('/admin/pelanggaran/{pelanggaran}', [PelanggaranController::class, 'show']);
Route::get('/admin/pelanggaran/delete/{id}', [PelanggaranController::class, 'destroy']);
//kategori
Route::get('/admin/kategori', [KategoriController::class, 'index']);
Route::get('/admin/kategori/create', [KategoriController::class, 'create']);
Route::post('/admin/kategori', [KategoriController::class, 'store']);
Route::get('/admin/kategori/delete/{id}', [KategoriController::class, 'destroy']);
Route::get('/admin/kategori/show/{kategori}', [KategoriController::class, 'show']);
//sanksi
Route::get('/admin/sanksi', [SanksiController::class, 'index']);
Route::get('/admin/sanksi/create', [SanksiController::class, 'create']);
Route::get('/admin/sanksi/update/{sanksi}', [SanksiController::class, 'edit']);
Route::post('/admin/sanksi/', [SanksiController::class, 'store']);
Route::put('/admin/sanksi/{sanksi}', [SanksiController::class, 'update']);
Route::get('/admin/sanksi/delete/{id}', [SanksiController::class, 'destroy']);
//gurupiket
Route::get('/admin/guru', [AdminController::class, 'gurupiket']);
Route::get('/admin/guru/delete/{id}', [AdminController::class, 'deletegurupiket']);
//wali
Route::get('/admin/wali', [AdminController::class, 'walimurid']);
//kelas
Route::get('/admin/kelas', [kelasController::class, 'index']);
Route::get('/admin/kelas/create', [kelasController::class, 'create']);
Route::post('/admin/kelas', [kelasController::class, 'store']);

Route::get('/admin/kelas/update/{kelas}', [kelasController::class, 'edit']);
Route::get('/admin/kelas/show/{kelas}', [kelasController::class, 'show']);
Route::get('/admin/kelas/delete/{id}', [kelasController::class, 'destroy']);


//siswa
Route::get('/admin/siswa', [SiswaController::class, 'index']);
Route::get('/admin/siswa/create', [SiswaController::class, 'create']);
Route::post('/admin/siswa', [SiswaController::class, 'store']);
Route::get('/admin/siswa/update/{siswa}', [SiswaController::class, 'edit']);
Route::put('/admin/siswa/{siswa}', [SiswaController::class, 'update']);
Route::get('/admin/siswa/delete/{id}', [SiswaController::class, 'destroy']);
Route::get('/admin/siswa/upload', [SiswaController::class, 'upload']);
Route::post('/admin/siswa/upload', [SiswaController::class, 'import'])->name('import');



//datapelanggaran
Route::get('/admin/datapelanggaran', [DatapelanggaranController::class, 'index']);

Route::get('/admin/tambahpelanggaran', [DatapelanggaranController::class, 'tambahpelanggaran']);
Route::POST ('/admin/simpanpelanggaran', [DatapelanggaranController::class, 'simpanpelanggaransiswa']);

Route::get('/guru/inputpelanggaran', [GuruController::class, 'tambahpelanggaran']);
Route::POST ('guru/simpanpelanggaran', [GuruController::class, 'simpanpelanggaransiswa']);


// Route::get('/guru/tambahpelanggaran', [DatapelanggaranController::class, 'tambahpelanggaran']);
// Route::POST ('/guru/simpanpelanggaran', [DatapelanggaranController::class, 'simpanpelanggaransiswa']);


//Rekomendasi Pelanggaran
Route::get('/admin/sanksipelanggaran' , [SanksiController::class , 'rekomendasi']);


//Guru
// Route::get('/guru/inputpelanggaran', [DatapelanggaranController::class, 'create']);
// Route::get('/guru/inputpelanggaran/pelanggaran/{siswa}', [DatapelanggaranController::class, 'pelanggaran']);
// Route::get('/guru/inputpelanggaran/pelanggaran/{siswa}/{pelanggaran}', [DatapelanggaranController::class, 'pelanggaran']);

//pesan Guru

Route::get('/guru/sanksi', [GuruController::class, 'rekomendasi']);
Route::get('/guru/pesan/', [GuruController::class, 'index']);
Route::get('/guru/pesan/kirim', [GuruController::class, 'createpesan']);
Route::post('/guru/pesan/kirim', [GuruController::class, 'storepesan']);
Route::get('/guru/pesan/show/{pesan}', [GuruController::class, 'showpesan']);
Route::get('/guru/pesan/delete/{id}', [GuruController::class, 'hapuspesan']);





Route::get('/guru/wali', [GuruController::class, 'walimurid']);

//pesan Wali
Route::get('/wali/pesan/', [WaliController::class, 'index']);

Route::get('/wali', [WaliController::class, 'rekomendasi']);

Route::post('/wali/pesan/kirim', [WaliController::class, 'storepesan']);
Route::get('/wali/pesan/kirim', [WaliController::class, 'createpesan']);
Route::get('/wali/pesan/balas/', [WaliController::class, 'balas']);
Route::post('/wali/pesan/balas/{id_penerima}', [WaliController::class, 'balaspesan']);
Route::get('/wali/pesan/show/{pesan}', [WaliController::class, 'showpesan']);

});