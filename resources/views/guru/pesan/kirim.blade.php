<!DOCTYPE html>
<html lang="en">

<head>
 @include('guru.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

           @include('guru.template.sidebar')

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
               @include('admin.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-800">Pesan</h1>
                @if (session('status'))

<div class="alert alert-success col-md-4">
    {{session('status')}}
</div>

@endif
        
                <form method="post" action="/guru/pesan/kirim">
                    @csrf
                    <div class="card-body col-md-6 ">
                    <div class="form-group">
                            
                            <input type="text" class=" form-control" id="subjek" placeholder="Masukkan Subjek" name="subjek" required >
                               
                        </div>
                   
                    <div class="mb-3">
                        
                        <select class="form-select form-select-sm form-control" aria-label=".form-select-sm example" name="id_penerima">
                            <option selected disabled value="">Wali Siswa</option>
                           @foreach($user as $u)
                            <option value={{$u->id}}>{{$u->nama}}</option>
                            @endforeach
                            </select>
                    </div>
                        <div class="mb-3">
                         
                            <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Masukkan Pesan Anda" rows="3" name="pesan"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                       <a href="{{url('guru/pesan')}}" class="btn btn-primary">Kembali</a>
                    </div>
               
                  

                </form>
          

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>