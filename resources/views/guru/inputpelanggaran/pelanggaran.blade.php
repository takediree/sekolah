<!DOCTYPE html>
<html lang="en">

<head>
 @include('guru.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

           @include('guru.template.sidebar')

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
               @include('guru.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                        <div class="card shadow mb-4">
                            <h1 class="h3 mb-2 text-gray-800">Daftar siswa dan sanksi</h1>

                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>N0</th>
                                                <th>Nama Siswa</th>
                                                <th>Poin</th>
                                                <th>Sanksi</th>


                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(!empty($sanksi))
                                                @foreach($sanksi->normalisasi_bobot as $s)
                                                    <tr>
                                                        <td scope="row">{{$loop->iteration}}</td>
                                                        <td>{{$s["nama"]}}</td>
                                                        <td>{{ $prefrensi[$loop->iteration-1] }}</td>
                                                        <td>{{ $rekomendasi[$loop->iteration-1] }}</td>


                                                    </tr>
                                                @endforeach
                                            @endif


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script>
      $(document).ready( function () {    


      $('#dataTable').DataTable();

      $(".pilih").click(function () {




        var _id =  $(this).data('id');
        var _status      =  $(this).data('status');
  
        var theToken    =  "{{ csrf_token() }}";




        $.ajax({
            type: "POST",
            dataType: 'text',
            url: '{{ url("admin/datapelanggaran/approve_tolak") }}',
            data: {_token: theToken, status:_status , id:_id},
            success: function (message) {

                
                location.reload();

        }
    });
      



});

} );
  </script>

</body>

</html>