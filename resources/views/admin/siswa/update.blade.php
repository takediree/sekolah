<!DOCTYPE html>
<html lang="en">

<head>
 @include('admin.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

           @include('admin.template.sidebar')

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
               @include('admin.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        
                <form method="post" action="{{ url("admin/siswa/".$siswa->id) }}" >
                    @csrf
                    @method('PUT')
          
                        <div class="form-group">
                            <label for="kelas" class="form-label">Kelas</label>
                           
                            <select class=" @error('kelas') is-invalid @enderror form-select form-control form-select-lg mb-3" aria-label=".form-select-lg example" type="text"  id="kelas" name="kelas" required >
                                    <option selected disabled value="{{$siswa->kelas}}">{{$siswa->kelas}}</option>
                                @foreach($kelas as $k)
                                    <option value={{$k->id}}>{{$k->kelas}}</option>
                                @endforeach
                            </select>
                                @error('kelas')
                                    <div class="invalid-feedback">
                                        inputan salah
                                    </div>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="nis"> Nis </label>
                            <input type="text" class="@error('nis') is-invalid @enderror form-control" id="nis" placeholder="Masukkan nis" name="nis" required  value="{{$siswa->nis}}">
                                @error('nis')
                                    <div  class="invalid-feedback">
                                        gunakan angka
                                    </div>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama"> nama </label>
                            <input type="text" class="@error('nama') is-invalid @enderror form-control" id="nama" placeholder="Nama Siswa" name="nama" value="{{$siswa->nama}}" required >
                                @error('nama')
                                    <div  class="invalid-feedback">
                                        gunakan angka
                                    </div>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin"> Jenis Kelamin </label>
                            <input type="text" class="@error('jenis_kelamin') is-invalid @enderror form-control" id="jenis_kelamin" placeholder="L / P" name="jenis_kelamin"  value="{{$siswa->jenis_kelamin}}"required >
                                @error('jenis_kelamin')
                                    <div  class="invalid-feedback">
                                        gunakan angka
                                    </div>
                                @enderror
                        </div>
              
                        <div class="form-group">
                            <label for="alamat"> Alamat </label>
                            <input type="text" class="@error('alamat') is-invalid @enderror form-control" id="alamat" placeholder="masukkan alamat" name="alamat"   value="{{$siswa->alamat}}" required >
                                @error('alamat')
                                    <div  class="invalid-feedback">
                                        gunakan angka
                                    </div>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="ttl"> Tanggal Lahir </label>
                            <input type="text" class="@error('ttl') is-invalid @enderror form-control" id="ttl" placeholder="tanggal/bulan/tahun" name="ttl"  value="{{$siswa->ttl}}"required >
                                @error('ttl')
                                    <div  class="invalid-feedback">
                                        gunakan angka
                                    </div>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="wali"> Wali Siswa</label>
                            <input type="text" class="@error('wali') is-invalid @enderror form-control" id="wali" placeholder="Wali Siswa" name="wali"   value="{{$siswa->wali}}"required >
                                @error('wali')
                                    <div  class="invalid-feedback">
                                        inputan salah
                                    </div>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="hp"> Telepon</label>
                            <input type="text" class="@error('hp') is-invalid @enderror form-control" id="hp" placeholder="no telepon wali siswa" name="hp"   value="{{$siswa->hp}}" required >
                                @error('hp')
                                    <div  class="invalid-feedback">
                                        gunakan angka
                                    </div>
                                @enderror
                        </div>
                      


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /.card-body -->



                </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>

</body>

</html>