<!DOCTYPE html>
<html lang="en">

<head>
 @include('admin.template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

           @include('admin.template.sidebar')

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
               @include('admin.template.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <h1 class="h3 mb-2 text-gray-800">Siswa</h1>
        
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <a href="/admin/siswa/create" type="button" class="btn btn-primary  btn-sm">Tambah Data</a>
                               
                                <a href="siswa/upload" class="btn btn-success btn-icon-split btn-sm">
                                    <span class="icon text-white-50">
                                        <i class="fa fa-upload"></i>
                                     </span>
                                     <span class="text">Upload</span>
                                </a>
                                <!-- <a href="siswa/upload" class="btn btn-success btn-icon-split btn-sm">
                                    <span class="icon text-white-50">
                                        <i class="fa fa-download"></i>
                                     </span>
                                     <span class="text">Pdf</span>
                                </a> -->
                                
                            </div>
                            

                            
                                @if (session('status'))

                                    <div class="alert alert-success col-md-4">
                                        {{session('status')}}
                                    </div>

                                @endif
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>NO</th>
                                                <th>kelas</th>
                                                <th>Nis</th>
                                                <th>Nama Siswa</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Alamat</th>
                                                <th>TTL</th>
                                                <th>Poin</th>
                                                <th>Opsi</th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach($siswa as $s)
                                                <tr>
                                                    <td scope="row">{{$loop->iteration}}</td>
                                                    <td>{{$kelas->kelas}}</td>
                                                    <td>{{$s->nis}}</td>
                                                    <td>{{$s->nama}}</td>
                                                    <td>{{$s->jenis_kelamin}}</td>
                                                    <td>{{$s->alamat}}</td>
                                                    <td>{{$s->ttl}}</td>                                                    
                                                    <td>10</td>                                                    
                                                    <td>

                                                    <a href="{{ url("admin/siswa/update",$s->id) }}" class="btn btn-info btn-icon-split btn-sm">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                        <span class="text">edit</span>
                                                    </a>
                                                    <a href="{{ url("admin/siswa/delete",$s->id) }}" class="btn btn-danger  btn-icon-split btn-sm"  onclick="return confirm('apakah anda yakin menghapus ?')">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-trash"></i>
                                                        </span>
                                                        <span class="text">hapus</span>
                                                    </a>
                                                       
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach        
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('sb/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('sb/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('sb/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('sb/js/sb-admin-2.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script>
      $(document).ready( function () {

    
    $('#dataTable').DataTable();

} );
  </script>

</body>

</html>