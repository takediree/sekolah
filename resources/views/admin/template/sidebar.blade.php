 <!-- Sidebar - Brand -->
 <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Admin <sup></sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Data tata tertib</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href="{{url('admin/kategori')}}">Kategori Pelanggaran</a>
                        <a class="collapse-item" href="{{url('admin/pelanggaran')}}">Bentuk Pelanggaran</a>
                        <a class="collapse-item" href="{{url('admin/sanksi')}}">Sanksi Pelanggaran</a>
                    </div>
                </div>
            </li>

            

             <li class="nav-item">
                 <a class="nav-link" href="{{url('admin/tambahpelanggaran')}}">
                     <i class="fas fa-fw fa-chart-area"></i>
                     <span>Tambah Pelanggaran</span></a>
             </li>


             <li class="nav-item">
                             <a class="nav-link" href="{{url('admin/sanksipelanggaran')}}">
                                 <i class="fas fa-fw fa-chart-area"></i>
                                 <span>Rekomendasi Sanksi </span></a>
                         </li>


 <!-- Nav Item - Utilities Collapse Menu -->


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Master Data
            </div>

            <!-- Nav Item - Pages Collapse Menu -->


            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/kelas')}}">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Kelas </span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/siswa')}}">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Siswa </span></a>
            </li>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/guru')}}">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Guru Piket</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/wali')}}">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Wali Siswa</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
