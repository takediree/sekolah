-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2021 at 07:38 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sekolah1`
--

-- --------------------------------------------------------

--
-- Table structure for table `datapelanggaran`
--

CREATE TABLE `datapelanggaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelanggaran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `bobot`, `created_at`, `updated_at`) VALUES
(1, 'Sikap / Perilaku', '0.34', '2021-03-08 08:19:22', '2021-03-08 08:19:22'),
(2, 'Kebersihan', '0.49', '2021-03-08 08:20:02', '2021-03-08 08:20:02'),
(4, 'Kerajinan', '0.16', '2021-03-08 08:20:19', '2021-03-08 08:20:19');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_siswa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `kelas`, `jumlah_siswa`, `created_at`, `updated_at`) VALUES
(1, 'Kelas II IPA 2', 12, '2021-03-08 07:25:15', '2021-03-08 07:25:15'),
(2, 'kelas II IPA 1', 12, '2021-03-08 08:02:39', '2021-03-08 08:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(14, '2020_12_11_065750_create_bengkel_table', 1),
(23, '2021_02_24_180342_create_datapelanggaran_table', 5),
(27, '2021_02_26_195159_drop_tabel_datapelanggaran', 8),
(54, '2014_10_12_000000_create_users_table', 9),
(55, '2021_01_06_031354_create_kategori_table', 9),
(56, '2021_02_23_142545_create_pelanggaran_table', 9),
(57, '2021_02_23_222022_create_sanksi_table', 9),
(58, '2021_02_24_191150_create_kelas_table', 9),
(59, '2021_02_24_223236_create_siswa_table', 9),
(60, '2021_02_26_195610_create_datapelanggaran_table', 9),
(61, '2021_03_06_172453_create_pesan_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `newdatapelanggaran`
--

CREATE TABLE `newdatapelanggaran` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_pelanggaran` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newdatapelanggaran`
--

INSERT INTO `newdatapelanggaran` (`id`, `id_siswa`, `id_pelanggaran`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-05-04 14:52:45', '2021-05-04 14:52:45'),
(2, 1, 1, '2021-05-04 15:47:26', '2021-05-04 15:47:26'),
(3, 1, 1, '2021-05-04 15:48:16', '2021-05-04 15:48:16'),
(4, 1, 1, '2021-05-04 16:12:49', '2021-05-04 16:12:49'),
(5, 2, 1, '2021-05-09 09:23:33', '2021-05-09 09:23:33'),
(6, 2, 3, '2021-05-09 09:26:57', '2021-05-09 09:26:57'),
(7, 1, 4, '2021-05-09 09:27:31', '2021-05-09 09:27:31'),
(8, 2, 4, '2021-05-09 09:27:35', '2021-05-09 09:27:35'),
(9, 1, 1, '2021-05-09 15:20:44', '2021-05-09 15:20:44'),
(10, 1, 1, '2021-05-10 03:10:06', '2021-05-10 03:10:06'),
(11, 3, 1, '2021-05-19 19:01:25', '2021-05-19 19:01:25'),
(12, 3, 4, '2021-05-19 19:01:32', '2021-05-19 19:01:32'),
(13, 3, 2, '2021-05-19 19:01:36', '2021-05-19 19:01:36'),
(14, 3, 3, '2021-05-19 19:06:58', '2021-05-19 19:06:58'),
(15, 3, 3, '2021-05-19 19:07:03', '2021-05-19 19:07:03'),
(16, 5, 1, '2021-07-05 17:36:18', '2021-07-05 17:36:18'),
(17, 5, 2, '2021-07-05 17:36:29', '2021-07-05 17:36:29'),
(18, 5, 3, '2021-07-05 17:37:30', '2021-07-05 17:37:30'),
(19, 2, 3, '2021-07-05 17:38:10', '2021-07-05 17:38:10'),
(20, 4, 3, '2021-07-05 17:38:31', '2021-07-05 17:38:31'),
(21, 4, 3, '2021-07-05 17:38:46', '2021-07-05 17:38:46'),
(22, 5, 5, '2021-07-05 18:11:38', '2021-07-05 18:11:38'),
(23, 2, 2, '2021-07-06 02:28:19', '2021-07-06 02:28:19'),
(24, 3, 1, '2021-07-06 02:33:07', '2021-07-06 02:33:07'),
(25, 1, 1, '2021-07-06 02:39:22', '2021-07-06 02:39:22'),
(26, 1, 1, '2021-07-06 02:40:29', '2021-07-06 02:40:29'),
(27, 3, 1, '2021-07-06 02:54:01', '2021-07-06 02:54:01'),
(28, 5, 5, '2021-07-06 02:55:44', '2021-07-06 02:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggaran`
--

CREATE TABLE `pelanggaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pelanggaran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poin` int(11) NOT NULL,
  `kategori` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggaran`
--

INSERT INTO `pelanggaran` (`id`, `pelanggaran`, `poin`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'Terlambat', 1, 4, '2021-03-08 08:21:46', '2021-03-08 08:21:46'),
(2, 'Tidak Menggunakan Dasi', 10, 2, '2021-05-08 00:24:59', '2021-05-08 00:24:59'),
(3, 'Bolos', 10, 1, '2021-05-09 01:26:48', '2021-05-09 01:26:48'),
(4, 'Tidak Piket', 10, 2, '2021-05-09 01:27:25', '2021-05-09 01:27:25'),
(5, 'berkelahi', 20, 1, '2021-05-19 11:13:41', '2021-05-19 11:13:41');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subjek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_penerima` int(11) NOT NULL,
  `id_pengirim` int(11) NOT NULL,
  `notifikasi` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `subjek`, `pesan`, `id_penerima`, `id_pengirim`, `notifikasi`, `created_at`, `updated_at`) VALUES
(1, 'aa', 'sasa', 1, 1, 0, '2021-03-16 10:18:04', '2021-03-16 10:18:04'),
(2, 'aa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 5, 0, 0, '2021-03-16 10:32:23', '2021-03-16 10:32:23'),
(3, 'aaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 3, 0, '2021-03-16 12:48:54', '2021-03-16 12:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `sanksi`
--

CREATE TABLE `sanksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sanksi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poin_batas` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sanksi`
--

INSERT INTO `sanksi` (`id`, `sanksi`, `poin`, `poin_batas`, `created_at`, `updated_at`) VALUES
(1, 'Tidak Perlu di berikan sanksi', '0', '0.25', '2021-03-10 09:48:32', '2021-03-10 09:48:32'),
(2, 'Dipanggil Orang Tua dan Skor 3 Hari', '0.26', '0.50', NULL, NULL),
(3, 'Dipanggil Orangtua dan Skor 6 Hari', '0.51', '0.75', NULL, NULL),
(4, 'Dikeluarkan dari sekolah', '0.76', '1', NULL, NULL),
(5, 'berkelahi', '10', NULL, '2021-07-05 09:58:53', '2021-07-05 09:58:53'),
(6, 'digappo', '5', '9', '2021-07-05 10:01:55', '2021-07-05 10:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wali` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `kelas`, `nis`, `nama`, `jenis_kelamin`, `alamat`, `ttl`, `wali`, `hp`, `created_at`, `updated_at`) VALUES
(1, '2', '1212', 'take', 'L', 'Jl. Urip Sumoharjo, Panaikang, Panakkukang, Makassar, Sulawesi Selatan 90233', 'Bone, 15 April 1994', 'sssss', 8522, '2021-03-08 08:23:24', '2021-03-08 08:23:24'),
(2, '2', '13020120313', 'Sam', 'L', 'Racing Centre', '11/11/1998', 'Wali', 123, '2021-05-08 00:24:05', '2021-05-08 00:24:05'),
(3, '1', '130201203133', 'Admin Fik 5', 'L', 'Alamatrtttttttttt', '11/11/1998', 'Wali', 8144444, '2021-05-19 11:01:13', '2021-05-19 11:01:13'),
(4, 'Kelas 2 IPA 2', '13', 'baco', 'L', 'jln tinumbu', 'pannampu 16/11/1995', 'anto', 2212, '2021-07-05 09:32:37', '2021-07-05 09:32:37'),
(5, 'Kelas 2 IPA 1', '123', 'cacce', 'L', 'jln sungai saddang baru', '17, january', 'sulaeman', 3032, '2021-07-05 09:32:37', '2021-07-05 09:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `nama`, `nip`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 2, 'guru', '1212', 'gurupiket@gmai.com', '$2y$10$vFFRBQY3RlXh28eLBiz21ODzJdxnpXKYU5Y6hot/j57aXO/unP316', '2021-03-08 08:27:15', '2021-03-08 08:27:15'),
(2, 1, 'take', '111', 'tak@gmail.com', '$2y$10$KWF2KPPB1htuFRHQEcqV/utFGzQ8A6oga4Q636RnlM.Fh6cK2Wsia', '2021-03-08 08:28:24', '2021-03-08 08:28:24'),
(3, 3, 'wali', '0852', 'wali@gmail', '$2y$10$qmiEpn/e1rxlT1I6uBvwUe4cu9I355YY3eB7gCCbMZXivkZgtRukK', '2021-03-16 10:00:42', '2021-03-16 10:00:42'),
(4, 3, 'wali1', '0000', 'wali1@gmail', '$2y$10$M5qaFmY4wbHWgcGitOo2XeP6qBkCCwIliSiQ1v2ByG00fmw59ukOG', '2021-03-16 10:29:08', '2021-03-16 10:29:08'),
(5, 2, 'guru1', '1111', 'guru1@gmail', '$2y$10$v11s/ZB.oO62Mz2UFdQ4C.q/IOXAqVASwa7w.xmy44hEATpoBXXGe', '2021-03-16 10:29:43', '2021-03-16 10:29:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `datapelanggaran`
--
ALTER TABLE `datapelanggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newdatapelanggaran`
--
ALTER TABLE `newdatapelanggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sanksi`
--
ALTER TABLE `sanksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nip_unique` (`nip`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datapelanggaran`
--
ALTER TABLE `datapelanggaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `newdatapelanggaran`
--
ALTER TABLE `newdatapelanggaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sanksi`
--
ALTER TABLE `sanksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
